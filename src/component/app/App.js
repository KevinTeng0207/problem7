import React, { Component } from 'react'
import './App.css';
import Heart from './Heart'
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      my_input_string: '',
      start: false
    };
    this.Input_text = this.Input_text.bind(this);
    this.Draw = this.Draw.bind(this);
  }

  Input_text = (e) => {
    // console.log(e.target.value)
    this.setState({
      my_input_string: e.target.value
    })
  }

  Draw = () => {
    this.setState({
      start: !this.state.start
    })
  }
  render() {
    return (
      <div className="App">
        {
          !this.state.start ?
            <div className="dis-center view-draw" >
              <div id="myModal" className="modal fade show">
                <div className="modal-dialog modal-draw">
                  <div className="modal-content">
                    {/* this is title */}
                    <div className="modal-header">
                      <h4 className="modal-title h4">Draw Heart Web</h4>
                    </div>
                    <div className="modal-body">
                      {/* this is input and submit button */}
                      <form className="form" method="post">
                        <div className="form-group">
                          <input data-testid="input_text" type="input" className="form-control input" name="text" placeholder="text" required="required" onChange={this.Input_text}></input>
                        </div>
                        <div className="form-group">
                          <button data-testid="input_button" type="button" className="btn btn-primary btn-lg btn-block" onClick={this.Draw}>Draw</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div >
            :
            <Heart text={this.state.my_input_string}></Heart>}
      </div>
    )
  }
}
