import React, { Component } from 'react'
import './Heart.css';
export default class heart extends Component {
  constructor(props) {
    super(props);
    this.draw = this.draw.bind(this);
  }


  draw = (text) => {
    const paddingStr = text + ' ';
    const M = Math;
    const pow = M.pow;
    // const sleep = (ms) => new Promise((resolve) => { setTimeout(() => { resolve() }, ms) });
    const gStdout = () => {
      let flag = -1;
      let len = paddingStr.length;
      return () => {
        if (flag >= len - 1) {
          flag = 0;
        } else {
          flag++;
        }
        return paddingStr[flag];
      }
    };

    const isIn = (x, y) => pow((pow((x * 0.05), 2) + pow((-y * 0.1), 2) - 1), 3) - pow((x * 0.05), 2) * pow((-y * 0.1), 3) < 0;
    const output = gStdout();

    const print = (x, y, text) => {
      let temp = text + (isIn(x, y) ? output() : ' ')
      // console.log(temp)
      return temp
    };

    let y = -15;
    let printtext = ''
    const myrender = () => {
      for (let x = -30; x < 30; x += 1) {
        printtext = print(x, y, printtext);
      }
      printtext += '\n'
      if (y < 10) {
        y++;
        myrender(printtext);
      }
      return printtext
    };
    let ans = myrender();
    return ans
  }

  render() {
    let { text } = this.props
    let final = ''
    if (text.length !== 0)
      final = this.draw(text)
    // console.log(final)
    return (
      <div className="dis-center view-draw">
        {
          text.length === 0 ?
            <div>no text !!!!</div>
            :
            <textarea rows="28" cols="55" readOnly defaultValue={final}></textarea>
        }
      </div>)
  }
}
