import { render, screen, waitFor } from '@testing-library/react';
import Heart from '../app/Heart';
// import userEvent from '@testing-library/user-event';

test('homepage', async () => {
    render(<Heart text={'123'} />);
    const textbox = screen.getByRole('textbox')
    // let text = screen.getByText(textbox.value.trim())
    await waitFor(() => {
        expect(textbox).toBeInTheDocument();
    });
});