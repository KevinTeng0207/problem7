import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import App from '../app/App';

test('homepage', () => {
  render(<App />);
  const linkElement = screen.getByText('Draw Heart Web');
  expect(linkElement).toBeInTheDocument();
});


test('homepage input', async () => {
  render(<App />);
  const inputField = screen.getByTestId(`input_text`);
  fireEvent.change(inputField, { target: { value: 'test' } })
  await waitFor(() => expect(inputField).toHaveDisplayValue('test'));
});


test('homepage button', async () => {
  render(<App />);
  const buttonField = screen.getByTestId(`input_button`);
  userEvent.click(buttonField)

  fireEvent.change(buttonField, { target: { value: 'test' } })
  expect(await screen.findByText("no text !!!!")).toBeInTheDocument()
});